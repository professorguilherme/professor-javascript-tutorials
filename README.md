# Javascript - Tutorials

## Descrição
Neste repositório você encontrará os códigos fontes usados nos vídeos tutoriais de Javascript.

## Description
In this repository you will find the source codes used in the Javascript tutorial videos.


# Precisando de Aulas Particulares? (Are you in need of Javascript Lessons?)
#### Veja o Vídeo (Watch the Video)
[![Assista o Video](https://img.youtube.com/vi/b6Y73ZTFkhM/0.jpg)](https://www.youtube.com/watch?v=b6Y73ZTFkhM)

Agende sua Aula Hoje Mesmo!
[Clique Aqui para Agendar suas Aulas de Javascript!](https://go.hotmart.com/K85458328X?dp=1)

Schedule your Class Today!
[Click Here to Schedule your Javascript Classes!](https://go.hotmart.com/K85458328X?dp=1)