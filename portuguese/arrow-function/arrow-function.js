/*
======================================================================================
Prof. Guilherme Pereira - Aulas Particulares de Javascript!
===========================================================
Tudo bem, pessoal?

Esta vídeo aula faz parte de uma série de vídeos sobre javascript, 
por isso, acesse a descrição para ver os demais vídeos já publicados e entender melhor os exemplos

* Caso tenha dificuldades para reproduzir o exemplo, deixe um comentário

** Obs: durante a captura de tela, evitarei usar acentos na palavras

Nesta vídeo aula estarei mostrando como criar arrow function do javascript e como usá-las

Like e Comentários
Seu like e comentário me mostram o que você quer aprender, por isso, se gosta do assunto deixe um like e/ou comentário

Tem Pressa para Aprender Javascript?
Participe das minhas aulas particulares e aprenda a programar em Javascript e Typescript!
======================================================================================
*/
// Exemplo:
// 1) criar e adicionar um botao na tela


// Como criar um função tradicional
function criaBotao() {
    let button = document.createElement('button');
    button.innerHTML = 'Meu Botao';
    window.document.body.appendChild(button);
}

criaBotao();

// Como criar uma arrow function
// vou reproduzir o mesmo algoritmo com arrow function
const criaOutroBotao = () => {
    let button = document.createElement("button");
    button.innerHTML = "Meu outro botao";
    window.document.body.appendChild(button);
}

// agora vou chamar a arrow function
criaOutroBotao();

// Como chamar funções
// 2) criar um campo de formulario
// vamos agora criar o segundo algoritmo so com a arrow function
// se preferir pode escrever em ingles
const criaCampoFormulario = (value) => {
    let field = document.createElement("input");
    field.setAttribute("type", "text");
    field.setAttribute("value", value);

    document.body.appendChild(field);
}

// Lembrando, e preciso chamar a funcao
criaCampoFormulario("meu valor");

// Espero que tenha gostado