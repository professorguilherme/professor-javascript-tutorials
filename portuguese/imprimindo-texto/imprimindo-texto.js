/*
======================================================================================
Prof. Guilherme Pereira - Aulas Particulares de Javascript!
===========================================================
Tudo bem, pessoal?

Esta vídeo aula faz parte de uma série de vídeos sobre javascript, 
por isso, acesse a descrição para ver os demais vídeos já publicados e entender melhor os exemplos

* Caso tenha dificuldades para reproduzir o exemplo, deixe um comentário

** Obs: durante a captura de tela, evitarei usar acentos na palavras

Nesta vídeo aula estarei mostrando como "imprimir" texto na tela e no console usando o javascript.
======================================================================================
*/

// Primeiro comando
console.log("meu texto");
// verifique primeiro se o nodejs esta "instalado" no seu pc, caso nao esteja, veja a playlist do canal

// Segundo comando: porem preciso de uma pagina arquivo para o teste
alert("meu alerta"); // cuidado, esta forma bloqueia o carregamento da pagina ate ser fechado o alerta

// Terceiro comando:
document.write("imprimindo texto no corpo da pagina"); // neste ultimo teste falho, pois o atributo defer, fala para o navegador carregar o javascript somente depois de terminar de carregar o html e o css, ou seja, dependendo do momento que executar este comando voce pode nao ter o que estava querendo realizar


// Espero que tenha gostado da aula, duvidas entre em contato e venha participar das aulas particulares