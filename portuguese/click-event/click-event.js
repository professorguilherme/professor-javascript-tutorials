/*
======================================================================================
Prof. Guilherme Pereira - Aulas Particulares de Javascript!
===========================================================
Tudo bem, pessoal?

Esta vídeo aula faz parte de uma série de vídeos sobre javascript, 
por isso, acesse a descrição para ver os demais vídeos já publicados e entender melhor os exemplos

* Caso tenha dificuldades para reproduzir o exemplo, deixe um comentário

** Obs: durante a captura de tela, evitarei usar acentos na palavras

Nesta vídeo aula estarei mostrando como usar o evento de clique do javascript com alguns exemplos
======================================================================================
*/
// Precisamos pegar um elemento html
const BUTTON = document.querySelector("#meu-botao");

console.log(BUTTON);
// Exemplo com function (anônima)
BUTTON.addEventListener("click", function(e) {
    BUTTON.innerHTML = "Trocando o texto do botao";
});

// Exemplo com arrow function 
BUTTON.addEventListener("click", (e) => {
    BUTTON.style.backgroundColor="#212121";
    BUTTON.style.borderColor="#ffffff";
    BUTTON.style.color="#ffffff";
    window.document.body.style.backgroundColor="#121212";
})

// Exemplo com function (nomeada)
function removeBotaoApos3Segundos(e) {
    setTimeout(() => {
        e.target.remove();
    }, 3000);
}

BUTTON.addEventListener("click", removeBotaoApos3Segundos);

// Sao 3 exemplos simples, mas com criatividade bem uteis
// Espero que tenha gostado, duvidas deixe um comentario
// ate o proximo tutorial